import React from 'react';
import NavSideBar from './components/NavSIdeBar/navsidebar'

function App() {
  return (
    <div className="App">
      <NavSideBar />
    </div>
  );
}

export default App;
