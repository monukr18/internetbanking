import React, { Component } from 'react'
import './transferWithInBank.css';
import { Input,Checkbox,Button } from 'antd';
import FinalTransfer from '../TransferFinal/finalTransfer'

export default class  transferWithInBank extends Component {
    state = {
        toogleContinue : false
    }
    handleContinue = () =>{
        this.setState({
            toogleContinue : true
        })
    }
    render() {
        return (
            <div>
                {this.state.toogleContinue ? <FinalTransfer /> : 
                    <>
                        <div className="type-transfer">
                <p className="mb-0">
                    <span class="font-weight-bold mr-4">Type Of Transfer</span>
                    <Input style={{width:"30%"}} size="small" placeholder="Transfer with in the bank" />
                </p>
                </div>
                <div className="fund-amount-transfer" style={{marginTop:"20px"}}>
                   <div className="row m-0">
                       <div className="col-md-6" style={{borderRight:"1px solid black"}}>
                           <div className="pt-4 pb-4">
                                <span class="font-weight-bold">From Account</span>
                                <Input className="mt-2 mb-2" size="small" placeholder="BOI - 14785" />
                                <p><span>Account Balance : </span> 
                                    <span class="font-weight-bold" style={{float:"right"}}>INR : 78451</span>
                                </p>
                           </div>
                           <div className="imp-note-style">
                           <p class="font-weight-bold">Important Note : </p>
                           <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that</p>
                           <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected</p>
                           <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical</p>
                           {/* <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that</p>
                           <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected</p> */}
                           
                           </div>
                       </div>
                       <div className="col-md-6">
                       <div className="pt-4 pb-4">
                                <span class="font-weight-bold">Receiver Account</span>
                                <Input className="mt-2 mb-2" size="small" placeholder="SBI - 58741" />
                                <p><span>Received Amount : </span> 
                                    <span class="font-weight-bold" style={{float:"right"}}>INR : 77891</span>
                                </p>
                                <span class="font-weight-bold">Transfer Description</span>
                                <Input className="mt-2 mb-2" size="small" placeholder="Lorem - Ipsum" disabled/>
                              
                                <span class="font-weight-bold">Transfer Type</span>
                                <Input className="mt-2 mb-2" size="small" placeholder="transfer now"/>
                                
                                <span class="font-weight-bold">Notify Me</span>
                                <Input className="mt-2 mb-2" size="small" placeholder="Mobile"/>
                                
                                <span class="font-weight-bold">Mobile Number</span>
                                <Input className="mt-2 mb-2" size="small" placeholder="9876543210"/>
                           </div>
                       </div>

                   </div>
                </div>
                <div className="terms-cond-box" style={{marginTop:"5px"}}>
                    <div className="row m-0">
                        <div className="col-md-6">
                        <Checkbox >I accept Terms & Conditions</Checkbox>
                        </div>
                        <div className="col-md-6">
                           <div style={{float:"right"}}>
                            <Button className="back-btn-css" type="primary">Back</Button>
                            <Button className="confirm-btn-css" type="primary" onClick={this.handleContinue}>Cofirm</Button>
                           </div>
                        </div>

                    </div>

                </div>
                    </>
                }
            </div>
        )
    }
}
