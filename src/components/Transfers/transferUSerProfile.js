import React, { Component } from 'react'
import './transfer.css';
import { Button } from 'antd';
import PortFolio from '../Images/portfolio.png'
import RightArrow from '../Images/rightArrow.png'
import SendMessage from '../Images/send.png'
export default class transferUSerProfile extends Component {
    render() {
        return (
            <div>
                <div className="user-profile-transfer">
                                <div className="row m-0">
                                    <div className="col-md-6">
                                        <div className="card-style">
                                            <span class="font-weight-bold">My Portfolio</span>
                                        </div>
                                        <div className="card-style">
                                            <span>This is transaction</span><br />
                                            <span class="font-weight-bold">INR 684.56</span><br />
                                            <span>28-11-2019</span>
                                        </div>
                                        <div className="card-style">
                                            <span>This is transaction</span><br />
                                            <span class="font-weight-bold">INR 684.56</span><br />
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div><img className="portfolio-image" src={PortFolio} alt="User Portfolio" /></div>
                                        <div className="portfolio-css"><p class="font-weight-bold">10.5 %</p><br /></div>
                                    </div>
                                </div>
                            </div>

                            <div className="Help-in-transfer" style={{ marginTop: "20px" }}>
                                <div className="row m-0">
                                    <div className="col-md-8">
                                        <span class="font-weight-bold">Help</span><br />
                                        <span>FAQ & Links</span>
                                    </div>
                                    <div className="col-md-4">
                                        <img style={{ width: "40%" }} src={RightArrow} alt="RightArrow" />
                                    </div>
                                </div>
                            </div>



                            <div className="customer-executive" style={{ marginTop: "20px" }}>
                                <div className="row m-0">
                                    <div className="col-md-12">
                                        <span class="font-weight-bold">Talk to our Customer Executive</span><br />
                                        <div style={{ paddingTop: "20px" }}>
                                            <a href="#" style={{ backgroundColor: "white", color: "black", border: "1px solid black" }} class="btn btn-primary btn-default">Start a Conversation <img style={{ width: "15%", float: "right" }} src={SendMessage} alt="RightArrow" /></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
            </div>
        )
    }
}
