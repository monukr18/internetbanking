import React, { Component } from 'react';
import './transfer.css'
import TransferMoney from '../Images/transfermoney.png'
import User from '../Images/user.png'
import TransferWithInBank from '../TransferWithInBank/transferWithInBank'
import TransferUserProfile from './transferUSerProfile'
export default class transfer extends Component {
    state = {
        toggleTransfer: false
    }
    handleTransferWithInBank = () => {
        this.setState({
            toggleTransfer: true
        })
    }
    render() {
        return (
            <div>
                {this.state.toggleTransfer ? <TransferWithInBank /> :
                    <div className="row m-0">
                        <div className="col-md-8">
                            <div className="transfer-option">
                                <div className="row m-0">
                                    <div className="col-md-4">
                                        <div onClick={this.handleTransferWithInBank} style={{ textAlign: "center", cursor: "pointer" }}>
                                            <p class="font-weight-bold">Transfer within the bank</p>
                                            <img style={{ width: "35%" }} src={TransferMoney} alt="Transfer Money" />
                                            <p style={{ marginTop: "1em" }}>PhasePlus Placeat</p>
                                        </div>
                                    </div>
                                    <div className="col-md-4"></div>
                                    <div className="col-md-4"></div>
                                </div>
                            </div>
                            {/* Beneficies */}
                            <div className="benefiecies-style" style={{ marginTop: "20px" }}>
                                <div className="row m-0">
                                    <div className="col-md-12">
                                        <div>
                                            <p class="font-weight-bold">Recently Used Beneficies</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-md-4">
                                        <div className="beneficied-user">
                                            <p style={{ marginBottom: "0", padding: "10px" }}>
                                                <img style={{ width: "20%", marginRight: "15px" }} src={User} />
                                                <span>Monu Kumar</span></p>
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <div className="beneficied-user">
                                            <p style={{ marginBottom: "0", padding: "10px" }}>
                                                <img style={{ width: "20%", marginRight: "15px" }} src={User} />
                                                <span>Monu Kumar</span></p>
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <div className="beneficied-user">
                                            <p style={{ marginBottom: "0", padding: "10px" }}>
                                                <img style={{ width: "20%", marginRight: "15px" }} src={User} />
                                                <span>Monu Kumar</span></p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <TransferUserProfile />

                        </div>

                    </div>
                }
            </div>
        )
    }
}
