import React, { Component } from 'react'
import { Layout, Menu, Breadcrumb, Icon } from 'antd';
import './navsidebar.css'
import 'antd/dist/antd.css';
import TransferProfile from '../Transfers/transferProfile'
export default class navsidebar extends Component {
    state = {
        toggle: true
    }
    handleOnTransfer = () => {
        this.setState({
            toggle: true
        })
        console.log("Hello Transfer Button Clicked")
    }
    render() {
        const { SubMenu } = Menu;
        const { Header, Content, Sider } = Layout;
        return (
            <div>
                <Layout>
                    <Header className="header">
                        <div className="logo" />
                        <Menu
                            theme="dark"
                            mode="horizontal"
                            defaultSelectedKeys={['2']}
                            style={{ lineHeight: '64px' }}
                        >
                            <Menu.Item key="1">nav 1</Menu.Item>
                        </Menu>
                    </Header>
                    <Layout>
                        <Sider width={200} style={{ background: '#fff' }}>
                            <Menu
                                mode="inline"
                                defaultSelectedKeys={['1']}
                                defaultOpenKeys={['sub1']}
                                style={{ height: '100%', borderRight: 0 }}
                            >
                                <SubMenu
                                    key="sub1"
                                    title={
                                        <span>
                                            <Icon type="home" />
                                            Home
                                        </span>
                                    }
                                >
                                </SubMenu>
                                <SubMenu
                                    key="sub2"
                                    title={
                                        <span>
                                            <Icon type="transaction" />
                                            Fund Transfer
                                        </span>
                                    }
                                >
                                </SubMenu>
                                <SubMenu
                                    key="sub3"
                                    title={
                                        <span>
                                            <Icon type="notification" />
                                            More
                                        </span>
                                    }
                                >
                                </SubMenu>
                            </Menu>
                        </Sider>
                        <Layout style={{ padding: '0 24px 24px' }} className="content">
                            <div style={{ margin: '6px 0' }}>
                                <h3 style={{ marginBottom: '2px' }}>Fund Transfer</h3>
                                <p style={{ marginBottom: '2px' }}>You are in fund transfer panel</p>
                            </div>
                            <div style={{ margin: '6px 0 6px' }}>
                                <span className="tab-style" onClick={this.handleOnTransfer}>Transfers</span>
                                <span className="tab-style">Add/Remove Beneficiary</span>
                                <span className="tab-style">Scheduled Transfers</span>
                                <span className="tab-style">Transaction History</span>
                            </div>
                            {this.state.toggle ? <div><TransferProfile /></div> : ""}
                            </ Layout >
                            </ Layout >
                            </ Layout >
            </div>
        )
    }
}
