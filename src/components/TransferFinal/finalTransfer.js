import React, { Component } from 'react'
import './finaltransfer.css'
import { Input,Checkbox,Button  } from 'antd';
import TransferUserProfile from '../Transfers/transferUSerProfile'
export default class finalTransfer extends Component {
    render() {
        return (
            <div>
                <div className="row m-0">
                    <div className="col-md-8">
                        <div className="transfer-type">
                            <p className="mb-0">
                                <span class="font-weight-bold mr-4">Type Of Transfer</span>
                                <Input style={{ width: "30%" }} size="small" placeholder="Transfer with in the bank" />
                            </p>
                        </div>
                        {/* Sender Account Details */}
                        <div className="fund-amount-transfer" style={{marginTop:"20px"}}>
                   <div className="row m-0">
                       <div className="col-md-6" style={{borderRight:"1px solid black"}}>
                           <div className="pt-4 pb-4">
                                <span class="font-weight-bold">Sender Account</span>
                                {/* <Input className="mt-2 mb-2" size="small" placeholder="BOI - 14785" /> */}
                                <p className="mt-2 mb-2"><span>From Account : </span> 
                                    <span class="font-weight-bold" style={{float:"right"}}>78963254178965</span>
                                </p>
                                <span class="font-weight-bold">Receiver Account</span>

                                <p className="mt-2 mb-2"><span>Beneficiary Name : </span> 
                                    <span class="font-weight-bold" style={{float:"right"}}>Monu Kumar</span>
                                </p>
                                <p className="mt-2 mb-2"><span>Account Number : </span> 
                                    <span class="font-weight-bold" style={{float:"right"}}>8596987455</span>
                                </p>
                                <p className="mt-2 mb-2"><span>IFSC Code : </span> 
                                    <span class="font-weight-bold" style={{float:"right"}}>BKID4699</span>
                                </p>

                                <p className="mt-2 mb-2"><span>Bank Name : </span> 
                                    <span class="font-weight-bold" style={{float:"right"}}>BOI</span>
                                </p>
                                <p className="mt-2 mb-2"><span>Transfer Description : </span> 
                                    <span class="font-weight-bold" style={{float:"right"}}>Credit Card Bill</span>
                                </p>
                                <p className="mt-2 mb-2"><span>Transfer Type : </span> 
                                    <span class="font-weight-bold" style={{float:"right"}}>Transfer Now</span>
                                </p>
                                <p className="mt-2 mb-2"><span>Communication Details : </span> 
                                    <span class="font-weight-bold" style={{float:"right"}}>abc@xyz.com</span>
                                </p>

                           </div>
                           {/* <div className="imp-note-style">
                           <p class="font-weight-bold">Important Note : </p>
                           <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that</p>
                           <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected</p>
                           <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical</p>
                           <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that</p>
                           <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected</p>
                           
                           </div> */}
                       </div>
                       <div className="col-md-6">
                       <div className="pt-4 pb-4">
                                {/* <span class="font-weight-bold">Receiver Account</span>
                                <Input className="mt-2 mb-2" size="small" placeholder="SBI - 58741" /> */}
                                <div className="row m-0 mb-3">
                                    <div className="col-md-6"><span>Transfer Amount : </span> </div>
                                    <div className="col-md-6"><span class="font-weight-bold">INR : 77891</span></div>
                                </div>
                               
                                <div className="row m-0 mb-3">
                                    <div className="col-md-6"><span>Bank Charges : </span> </div>
                                    <div className="col-md-6"><span class="font-weight-bold">INR : 7</span></div>
                                </div>
                               
                                <div className="row m-0 mb-3">
                                    <div className="col-md-6"><span>Total : </span> </div>
                                    <div className="col-md-6"><span class="font-weight-bold">INR : 77898</span></div>
                                </div>
                               
                                <div className="row m-0">
                                    <div className="col-md-12">
                                        <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that</p>
                                    </div>
                                </div>
                                <div className="row m-0 mb-3">
                                    <div className="col-md-4">
                                        <span>Email : </span>
                                    </div>
                                    <div className="col-md-8">
                                     <Checkbox >abc@xyz.com</Checkbox> <br />
                                    </div>
                                </div>
                                    <h1>Alexa Learning with </h1>
                                <div className="row m-0 mb-3">
                                    <div className="col-md-4">
                                        <span>Mobile : </span>
                                    </div>
                                    <div className="col-md-8">
                                     <Checkbox >985632147</Checkbox> <br />
                                    </div>
                                </div>
                                <div className="row m-0">
                                    <div className="col-md-12"><Button className="otp-btn-css" type="primary">Generate OTP</Button></div>
                                </div>
                           </div>
                       </div>

                   </div>
                </div>
                <div className="terms-cond-box" style={{marginTop:"5px"}}>
                    <div className="row m-0">
                        <div className="col-md-6">
                       
                        </div>
                        <div className="col-md-6">
                           <div style={{float:"right"}}>
                            <Button className="back-btn-css" type="primary">Back</Button>
                            <Button className="confirm-btn-css" type="primary">Cofirm</Button>
                           </div>
                        </div>
                    </div>
                </div>
                        
                    </div>
                    <div className="col-md-4">
                        <TransferUserProfile />
                    </div>

                </div>
                
            </div>
        )
    }
}
